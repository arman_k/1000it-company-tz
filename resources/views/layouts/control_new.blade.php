@extends('layouts.app')

@section('content')
    <div class="cabinet-page-wrapper" id="vue-container">
        @yield('content_head')
        @hasSection('content_body')
            <div class="container-body-wrapper">
                <div class="container-content-container">
                    <div class="site-body__columns-wrapper">
                        <div class="site-body__left-column">
                            <div class="cabinet-links" style="display: none">
                                <a class="link link_thm_aqua" href="#">{!! trans('main.quest_need_help') !!}</a>
                            </div>
                        </div>
                        <div class="site-body__right-column" id="schools-list">
                            @yield('content_body')
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @yield('content_footer')
    </div>
@endsection

