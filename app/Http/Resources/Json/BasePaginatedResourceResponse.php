<?php

namespace App\Http\Resources\Json;

use Illuminate\Http\Resources\Json\PaginatedResourceResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\UrlWindow;
use Illuminate\Support\Arr;

class BasePaginatedResourceResponse extends PaginatedResourceResponse
{
    /**
     * Add the pagination information to the response.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function paginationInformation($request)
    {
        $data = $request->except('filters');
        /** @var LengthAwarePaginator $paginator */
        $paginator = $this->resource->resource;
        //TODO расмотреть этот варианта как более удобный для формирования сыллок в vue
        $links = [];
        if ($paginator->hasPages()) {
            if (!empty($data)) {
                $paginator->appends($data);
            }
            $links[] = [
                'disabled' => $paginator->onFirstPage(),
                'text' => '&lsaquo;',
                'link' => $paginator->previousPageUrl(),
            ];
            foreach ($this->elements($paginator) as $element) {
                if (is_string($element)) {
                    $links[] = [
                        'text' => $element,
                        'link' => null,
                    ];
                } elseif (is_array($element)) {
                    foreach ($element as $page => $url) {
                        $links[] = [
                            'text' => $page,
                            'link' => $url,
                            'current' => ($page == $paginator->currentPage()),
                        ];
                    }
                }
            }
            $links[] = [
                'disabled' => ! $paginator->hasMorePages(),
                'text' => '&rsaquo;',
                'link' => $paginator->nextPageUrl(),
            ];
        }
        $paginated = $this->resource->resource->toArray();

        return [
            'links' => $this->paginationLinks($paginated),
            'generated_links' => $links,
            'meta' => $this->meta($paginated) + ['pageName' => $paginator->getPageName()],
        ];
    }

    /**
     * Gather the meta data for the response.
     *
     * @param array $paginated
     * @return array
     */
    protected function meta($paginated)
    {
        return Arr::except(
            $paginated,
            [
                'data',
            ]
        );
    }

    /**
     * Get the array of elements to pass to the view.
     *
     * @param LengthAwarePaginator $paginator
     * @return array
     */
    protected function elements($paginator)
    {
        $window = UrlWindow::make($paginator);

        return array_filter(
            [
                $window['first'],
                is_array($window['slider']) ? '...' : null,
                $window['slider'],
                is_array($window['last']) ? '...' : null,
                $window['last'],
            ]
        );
    }
}
