<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\AuthorsResource;
use App\Models\Author;
use App\Http\Controllers\Controller;

class AuthorsController extends Controller
{
    public function index()
    {
        return AuthorsResource::collection(Author::all());
    }
}
